////////////////////////////////////////////////////////////////////////////////////
//          VolGIS - A new volcano-oriented Geographic Information System         //
//             A Project made by Roberto Guardo (www.robertoguardo.eu)            //
////////////////////////////////////////////////////////////////////////////////////

// VolGIS CC BY-NC-SA [More info at https://creativecommons.org/licenses/] 
// 
//
// Directed by:
//    Roberto Guardo
//    Carola Dreidemie (Laboratorio de ID+i en Visualización, Computación Gráfica y Código Creativo - UNRN)
//
//
// Coded by:
//    Roberto Guardo    day 0 --> ongoin
//    Carola Dreidemie  day 0 --> ongoin
//    Ariel Uzal        day 0 --> 04/2017
//    Fernán Inchaurza  16/04/2018 --> ongoin 
//    
//  
// VolGIS 2018 Ver. 1.0 -- Presented at EGU2018 -- https://meetingorganizer.copernicus.org/EGU2018/EGU2018-221.pdf 
// 
// VolGIS 2018 Ver. 2.0 -- Presented at CoV10   -- https://www.citiesonvolcanoes10.com/wp-content/uploads/2018/08/S01.05-03-09-18-SiciliaDEF-3.pdf


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

/* 
 
 GRILLA_22
 
 + THE GICENTRE UTILS LIBRARY IS NECESSARY TO PROJECT THE MAP (http://www.gicentre.net/utils/)
 
 + NOTA: SI LOS PUNTOS SE GRAFICAN CON ESCALA MENOR, BUSCAR EL SIGUIENTE RENGLON EN EL CSV Y BORRARLO: 4/8/10,23:13:06.44,38.415,14.953,0.81,0.50
 
 ---------------------------------------------------------------------------------------
 
 */

import nervoussystem.obj.*;             // To export 3D objects (ndRG: not working)
import org.gicentre.utils.spatial.*;    // Necessary to map projections.


UTM proj;

Tomography t;

Menu menu;

Coord c;

Ruler ruler;

Picker picker;

IsoSurface iso;

boolean ed50=true;        //Switch to false to use the WGS84 projection system
int shown=0;
boolean first;

PShape info_svg;

PFont font;
PFont ui_font;
PFont quick;

int divisions = 5;
float division_side;
float cubes;
PGraphics render;

Table table;
Table table_clean;

ArrayList <Point> p;
int point_select = 0;

int select_index = 0;

PVector [] isopoints = new PVector [0];

boolean small_window = false;

ArrayList <GumballCube> gumcubes;

//************************************camera

int delta_cam_x;
int delta_cam_y;

int delta_pan_y;
int delta_pan_x;

int pan_x;
int pan_y;

float rot_x = 0;
float rot_y = 0;
float zoom = 0;

boolean resetting_cam = false;
boolean mouse_cam;
boolean record;
float cam_lerp_control = 0;
float rot_x_aux;
float rot_y_aux;
float zoom_aux;

//+++++++++++++ Parameter for the orthoView

//float left = -width/2; 
//float right = width/2;
//float bottom = -height/2;
//float top = height/2;
float near;
float far;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++dem

float[][] alt = new float[10000][10000];
float minAlt;
float maxAlt; 
PGraphics pg;
int columnas;
int filas;
int nodata, cellsize;
float coordx;
float coordy;
float x2, y2, z2;
int scale=10;
int def=25;

//**************************************************************** 4d

int maxYear=0, maxMonth=0, maxDay=0, minYear=3000, minMonth=12, minDay=31, m, prevM;
boolean manualAnimation;
int timeStartDay, timeStartMonth, timeStartYear, timeEndDay, timeEndMonth, timeEndYear, cantDias;//,timelineWindow=6

void settings() {

  if (displayHeight < 1080 ) {
    small_window = true;
  }

  if (small_window) {

    size(1300, 720, P2D);
  } else {

    size(1600, 1000, P2D);
  }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void setup() {

  c = new Coord();

  loadExternal();  // load table and typography
  
  Ellipsoid inter = new Ellipsoid(Ellipsoid.INTERNATIONAL);  // init projection library

  proj = new UTM(inter, 33, 'N'); // init projection library

  initRender();

  computePoints();  // earthquakes loading and analisis 

  resetCamera();

  picker = new Picker(render.width, render.height);  

  gumcubes = new ArrayList();

  t = new Tomography();  // tomography load and processing

  //******************************************************************** menu creation
  
  menu = new Menu(0, 20);

  // menu.addSliderComplex("subdivisiones", 1, 200, 5, true); // Ori
  menu.addSliderComplex("Subdivisions", 1, 200, 99, true); // Events with ERH-ERZ <= 0,5
  menu.addInfo("Meters per side:");
  menu.addInfo("Meters per division:");
  menu.addInfo("Number of events (CropBox):");
  menu.addToggle("Show axis", false);
  menu.addToggle("Show grid", false);
  menu.addToggle("Show sea level", false);
  menu.addToggle("Gradient subdivisions", true);
  menu.addSeparator();
  //menu.addCamControl();
  menu.addToggle("Free movement", true);
  menu.addToggle("Reset Cam", false);
  menu.addSeparator();
  menu.addToggle("Analyze minima", false);
  menu.addSliderComplex("Minima threshold", 1, 50, 15, true);
  menu.addToggle("Analyze maxima", false);
  menu.addSliderComplex("Maxima threshold", 3, 50, 16, true);
  menu.addSeparator();
  menu.addToggle("Compute and show isoSurface", false);
  menu.addSliderComplex("Isosurface threshold", 0, 20, 0, false); //ori
  menu.addSeparator();
  menu.addToggle("Show earthquakes", true);
  menu.addInfo("Shown earthquakes:");
  menu.addSliderComplex("Magnitude threshold", 0, 10, 0, false); 
  menu.addToggle("Start animation", false);
  menu.addTextboxYear("starting year", true, str(minYear));
  menu.addTextboxMonth("starting month", true, str(minMonth));
  menu.addTextboxDay("starting day", true, str(minDay));
  menu.addTextboxYear("ending year", true, str(maxYear)); 
  menu.addTextboxMonth("ending month", true, str(maxMonth));
  menu.addTextboxDay("ending day", true, str(maxDay));
  //  menu.addToggle("year", false);
  menu.addSliderComplex("year", minYear, maxYear, maxYear, true); 
  //  menu.addToggle("month", false);
  menu.addSliderComplex("month", 1, 12, maxMonth, true); 
  //  menu.addToggle("day", false);
  menu.addSliderComplex("day", 1, 31, maxDay, true); 

  menu.addSeparator();
  menu.addToggle("Crop box", false);
  menu.addToggle("Apply to isosurface", false);
  menu.addToggle("Add subcube", false);
  menu.addTextbox("min. long", true, str(c.min_lon_deg));    //14.95
  menu.addTextbox("max. long", true, str(c.max_lon_deg));    //15.15
  menu.addTextbox("min. lat", true, str(c.min_lat_deg));     //37.68
  menu.addTextbox("max. lat", true, str(c.max_lat_deg));     //37.78
  menu.addTextbox("min. depth (Km)", true, str(c.min_pro));  //-2
  menu.addTextbox("max. depth (Km)", true, str(c.max_pro));  //8
  /*----------------------------------------------------------------------------- EtnaCropBox
   menu.addTextbox("min. long", true, "14.95");
   menu.addTextbox("max. long", true, "15.15");
   menu.addTextbox("min. lat", true, "37.68");
   menu.addTextbox("max. lat", true, "37.78");
   menu.addTextbox("min. depth (Km)", true, "-2");
   menu.addTextbox("max. depth (Km)", true, "8");
   -------------------------------------------------------------------------------------------*/
  menu.addSeparator();
  menu.addToggle("Ortho", false);
  menu.addSeparator();
  menu.addToggle("Tomography Section", false);
  menu.addToggle("Top Layer", true);
  menu.addToggle("Horizontal -2 b.s.l.", false);
  menu.addToggle("Horizontal -1 b.s.l.", false);
  menu.addToggle("Horizontal 0", false);
  menu.addToggle("Horizontal 1 b.s.l.", false);
  menu.addToggle("Horizontal 2 b.s.l.", false);
  menu.addToggle("Horizontal 3 b.s.l.", false);
  menu.addSeparator();
  menu.addToggle("Tomography Vertical Section", false);
  menu.addSeparator();
  menu.addToggle("Active rule", false);
  menu.setFont(ui_font);
  menu.calculateScroll();
  //-------------------------------------------------------------------------------------------

  createIsoSurface();  // creacion de isosurface

  ruler = new Ruler();
  ruler.setFont(ui_font);

  //c.demLoad();

  println("\n••setup••end••\n");
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void draw() 
{
  shown=0;

  if (record)
    beginRecord("nervoussystem.obj.OBJExport", "VolGIS.obj");//captura de cuadros

  surface.setTitle(str(frameRate));

  divisions = menu.getSliderValueInt("Subdivisions");
  division_side = (cube_side*1.0)/divisions;
  cubes = pow(divisions, 3); //cantidad de subcubos

  if (menu.getSliderFlag("subdivisiones") || menu.getSliderFlag("Isosurface threshold") || menu.getToggleFlag("Compute and show isosurface")) //recrear isosurface cuando hay cambios en el menu
  {
    if (menu.getToggleState("Compute and show isosurface")) 
    {
      createIsoSurface();
      menu.lowerToggleFlag("Compute and show isosurface");
    }
  }

  if (menu.getSliderValueInt("Minima threshold") > menu.getSliderValueInt("Maxima threshold"))//ajustar los tresholdspara que no se superpongan 
    menu.setSliderValue("Maxima threshold", menu.getSliderValueInt("Minima threshold") + 1);

  density_high_thresh = menu.getSliderValueInt("Maxima threshold");
  density_low_thresh = menu.getSliderValueInt("Minima threshold");

//+++++++++++++++++++++++++++++++++++++++++++++++ movimiento de camara

  if (mouseX > 300  && mouse_cam  && mouseButton == LEFT) //chequear que el mouse estee en la pantalla render para mover la camara
  {
    delta_cam_x = mouseX - pmouseX;
    delta_cam_y = mouseY - pmouseY;
  } 
  else 
  {
    delta_cam_x = 0;
    delta_cam_y = 0;
  }

  if (mouseX > 300  && mouse_cam  && mouseButton == RIGHT) //chequear que el mouse estee en la pantalla render para mover la camara
  {
    delta_pan_x = mouseX - pmouseX; 
    delta_pan_y = mouseY - pmouseY;
    //camera(mouseX, height/2, (height/2) / tan(PI/6), mouseX, height/2, 0, 0, 1, 0);
  } 
  else 
  {
    delta_pan_x = 0; 
    delta_pan_y = 0;
  }

  if (menu.getToggleState("Free movement")) //check si se puede manejar la camara con el mouse
  {
    rot_x += delta_cam_x;
    rot_y -= delta_cam_y;
    pan_x -= delta_pan_x;
    pan_y -= delta_pan_y;
  }

  zoom += map(menu.getCamZoom(), -1, 1, -10, 10);
  zoom = constrain(zoom, 1, 4000);
  
  rot_x += map(menu.getCamPanX(), -1, 1, -2, 2);
  rot_y -= map(menu.getCamPanY(), -1, 1, -2, 2);
  rot_y = rot_y % 360;
  rot_x = rot_x % 360;
  
//+++++++++++++++++++++++++++++++++++++++++++++++

  picker.prepareBufferer();

  picker.setBufferCamera(pan_x, pan_y, zoom, pan_x, pan_y, 0.0, 0.0, 1.0, 0.0);
  
  for (int i = 0; i < gumcubes.size(); i ++)//draw gumball cube
    gumcubes.get(i).renderBuffer(picker.buffer);

  picker.closeBuffer();

  for (int i = 0; i < gumcubes.size(); i ++)//check hover of gumball cubes
    gumcubes.get(i).checkHovers();
// ------------------------------------------------------------------------------ dibujar pantalla render
  render.beginDraw();
  if (menu.getToggleState("Ortho"))//ORTHOGRAPHIC VIEW
  {
    //render.ortho();
    render.ortho(-cube_side/3, cube_side/3, -cube_side/3, cube_side/3);           //must correct the rule NdRtn
    picker.buffer.ortho();
  }
  else
  {
    render.perspective();
    picker.buffer.perspective();
  }
  
  render.lights();
  render.background(0);

  render.camera(pan_x, pan_y, zoom, pan_x, pan_y, 0.0, 0.0, 1.0, 0.0);//posicion camara

  render.pushMatrix();
  render.translate(render.width/2, render.height/2);
  render.rotateX(radians(rot_y));
  render.rotateY(radians(rot_x));

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++limites sliders 4d  (desprolijo)
  if (int(menu.getSliderValue("day"))>28&&int(menu.getSliderValue("month"))==2)
  {
    menu.setSliderValue("day", 28);
    if (int(menu.getSliderValue("year"))%4==0)
      menu.setSliderValue("day", 29);
  }

  if (int(menu.getSliderValue("day"))>30&&(int(menu.getSliderValue("month"))==4||int(menu.getSliderValue("month"))==6||int(menu.getSliderValue("month"))==9||int(menu.getSliderValue("month"))==11))
    menu.setSliderValue("day", 30);

  if (int(menu.getSliderValue("day"))<int(menu.getTextboxCont("starting day"))&&int(menu.getSliderValue("month"))==int(menu.getTextboxCont("starting month"))&&int(menu.getSliderValue("year"))==int(menu.getTextboxCont("starting year")))
    menu.setSliderValue("day", int(menu.getTextboxCont("starting day")));

  if (int(menu.getSliderValue("month"))<int(menu.getTextboxCont("starting month"))&&int(menu.getSliderValue("year"))==int(menu.getTextboxCont("starting year")))
    menu.setSliderValue("month", int(menu.getTextboxCont("starting month")));

  if (int(menu.getSliderValue("year"))<int(menu.getTextboxCont("starting year")))
    menu.setSliderValue("year", int(menu.getTextboxCont("starting year")));

  if (int(menu.getSliderValue("day"))>int(menu.getTextboxCont("ending day"))&&int(menu.getSliderValue("month"))==int(menu.getTextboxCont("ending month"))&&int(menu.getSliderValue("year"))==int(menu.getTextboxCont("ending year")))
    menu.setSliderValue("day", int(menu.getTextboxCont("ending day")));

  if (int(menu.getSliderValue("month"))>int(menu.getTextboxCont("ending month"))&&int(menu.getSliderValue("year"))==int(menu.getTextboxCont("ending year")))
    menu.setSliderValue("month", int(menu.getTextboxCont("ending month")));

  if (int(menu.getSliderValue("year"))>int(menu.getTextboxCont("ending year")))
    menu.setSliderValue("year", int(menu.getTextboxCont("ending year")));

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++fin limites sliders 4d
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++dibujar terremotos

  if (menu.getToggleState("Show earthquakes")&&!menu.getToggleState("Start animation"))
  {
    timeStartDay=int(menu.getTextboxCont("starting day"));
    timeStartMonth=int(menu.getTextboxCont("starting month"));
    timeStartYear=int(menu.getTextboxCont("starting year"));
    first=true;
    for (int i = 0; i < p.size(); i++)
    {
      if (p.get(i).mag >= menu.getSliderValue("Magnitude threshold")//condiciones para dibujar terremotos fuera de la animacion
        &&
        (p.get(i).year  <   int(menu.getSliderValue("year"))          ||
        (p.get(i).month <   int(menu.getSliderValue("month"))         && p.get(i).year  == int(menu.getSliderValue("year")))          ||
        (p.get(i).day   <=  int(menu.getSliderValue("day"))           && p.get(i).month == int(menu.getSliderValue("month"))          && p.get(i).year == int(menu.getSliderValue("year")))
        )&&
        (p.get(i).year  >   int(menu.getTextboxCont("starting year"))   ||
        (p.get(i).month >   int(menu.getTextboxCont("starting month"))  && p.get(i).year   == int(menu.getTextboxCont("starting year")))  ||
        (p.get(i).day   >=  int(menu.getTextboxCont("starting day"))    && p.get(i).month  == int(menu.getTextboxCont("starting month"))  && p.get(i).year == int(menu.getTextboxCont("starting year")))
        ))
      {
        if (i== point_select)
          p.get(i).select();
        else 
          p.get(i).deselect();
        shown++;
        p.get(i).render(render);
      }
    }
  }

  if (menu.getToggleState("Show earthquakes")&&menu.getToggleState("Start animation")) 
  {
//***************************************************avance de tiempo para la animacion
    first=false;
    if (!manualAnimation)
      timeStartDay=timeStartDay+1;
    while (timeStartDay>31)
    {
      timeStartMonth++;
      timeStartDay=timeStartDay-31;
    }
    while (timeStartMonth>12)
    {
      timeStartYear++;
      timeStartMonth=timeStartMonth-12;
    }
    timeEndDay=timeStartDay;

    timeEndMonth=timeStartMonth;

    timeEndYear=timeStartYear;
//***************************************************fin avance de tiempo para la animacion

    if (timeStartDay   >=  int(menu.getTextboxCont("ending day"))    && timeStartMonth  == int(menu.getTextboxCont("ending month"))  && timeStartYear == int(menu.getTextboxCont("ending year")))//terminar animacion
      menu.uncheckToggle("Start animation");

    for (int i = 0; i < p.size(); i++) 
    {
      if (p.get(i).mag >= menu.getSliderValue("Magnitude threshold")//condiciones para dibujar terremotos durante la animacion
        &&
        (p.get(i).year  >   int(menu.getTextboxCont("starting year"))   ||
        (p.get(i).month >   int(menu.getTextboxCont("starting month"))  && p.get(i).year  == int(menu.getTextboxCont("starting year")))  ||
        (p.get(i).day   >=  int(menu.getTextboxCont("starting day"))    && p.get(i).month == int(menu.getTextboxCont("starting month"))  && p.get(i).year == int(menu.getTextboxCont("starting year")))
        )&&
        (p.get(i).year  <   timeEndYear     ||
        (p.get(i).month <   timeEndMonth    && p.get(i).year   == timeEndYear)   ||
        (p.get(i).day   <=  timeEndDay      && p.get(i).month  == timeEndMonth   && p.get(i).year == timeEndYear)
        ))
        {
        if (i== point_select)
          p.get(i).select();
        else
          p.get(i).deselect();

        shown++;
        p.get(i).render(render);
      }
    }
  }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++dibujar terremotos
  if (menu.getToggleState("Show axis"))
    drawHelperGuides();
  else
    drawNorthArrow();

  if (menu.getToggleState("Show grid")) 
  {
    if (menu.getToggleState("Gradient subdivisions"))
      drawGradientDivisions();
    else
      drawDivisions();
    drawBoundingCube();
  }
  if (menu.getToggleState("Show sea level")) 
    drawSeaPlane();

  if (menu.getToggleState("Compute and show isosurface")) 
    drawIsoSurface();

  if (menu.getToggleState("Analyze minima") || menu.getToggleState("Analyze maxima")) 
    analyzeDensity();

  if (menu.getToggleState("Tomography Section")) 
    t.renderHorizontals(render);
  
  if (menu.getToggleState("Tomography Vertical Section")) 
    t.renderVerticals(render);

  if (menu.getToggleState("Reset Cam"))//volver camara al origen
  {
    fireCameraReset();
    menu.uncheckToggle("Reset Cam");
  }   

  if (menu.getToggleState("Add subcube")) 
  {
    addSubCube();
    menu.uncheckToggle("Add subcube");
  }   

  checkCropValues();

  if (menu.getToggleState("Crop box")) //dibujar cropbox
  {
    c.findCropBoxCoords();
    renderCropBox();
  }  


  if (resetting_cam)//volviendo camara al origen
  {
    rot_x = lerp(rot_x_aux, init_rot_x, quinticOut(cam_lerp_control));
    rot_y = lerp(rot_y_aux, init_rot_y, quinticOut(cam_lerp_control));
    zoom  = lerp(zoom_aux, init_zoom, quinticOut(cam_lerp_control));
    if (cam_lerp_control <1)
      cam_lerp_control += 0.005;
    else 
    {
      resetting_cam = false;
      cam_lerp_control = 0;
    }
  }

  int info_panel_acum = 0;

  for (int i = 0; i < gumcubes.size(); i ++) //dibujar gumball cubes
  {
    gumcubes.get(i).update(render);
    gumcubes.get(i).render(render);
    if (gumcubes.get(i).display_info) 
    {
      gumcubes.get(i).info.setPosition(gumcubes.get(i).info.x, info_panel_acum);
      info_panel_acum += gumcubes.get(i).info.h;
    }
  }
  /*stroke(255);
  float x =  map(coordx/1000, c.min_lon_map, c.max_lon_map, -cube_side/2, cube_side/2);
  float y =  map(0, c.min_prof_map, c.max_prof_map, -cube_side/2, cube_side/2);
  float z =  map(coordy/1000, c.min_lat_map, c.max_lat_map, -cube_side/2, cube_side/2);
  x2=map((coordy+filas*cellsize)/1000, c.min_lat_map, c.max_lat_map, -cube_side/2, cube_side/2);
  y2=map(0, c.min_prof_map, c.max_prof_map, -cube_side/2, cube_side/2);
  z2=map((coordy+columnas*cellsize)/1000, c.min_lat_map, c.max_lat_map, -cube_side/2, cube_side/2);
  //println(x, "    ", y, "    ", z, "    ", x2, "    ", y2, "    ", z2);
  println((coordx+columnas*cellsize)/1000,"    ",c.min_lon_map,"    ",c.max_lon_map);
  render.line(x, y, z, x2, y2, z2);
  render.pushMatrix();
  render.translate(x, y, z);
  render.sphere(15);
  render.popMatrix();
  /*
  render.pushMatrix();
  render.translate(x2,y2,z2);
  render.sphere(15);
  render.popMatrix();
  */
  render.popMatrix();

  render.endDraw();

  background(30);

  image(render, 300, 0);

  if (menu.getToggleState("Active rule"))//inicial y usar regla
  {
    if (!ruler.active)
      menu.checkToggle("Ortho");
    if (checkRulerRequisites())
    {
      ruler.activate();
      ruler.update();
      ruler.render();
    }
  }
  else
    ruler.deactivate();

  menu.update();

  menu.render();

  for (int i = 0; i < gumcubes.size(); i ++)//mostrar info gumball cubes
    gumcubes.get(i).manageInfoPanel();

  updateMenuInfo();
  //if(menu.getToggleState("Start animation")&&menu.getToggleState("Show earthquakes"))
  
//********************************************************************dibujo de la barra temporal y las lineas para separar años y meses
  int tmp=width-300, buffer=31-int(menu.getTextboxCont("starting day")), bufferMonth, startDay, endDay, bufferYear;
  cantDias=(31-int(menu.getTextboxCont("starting day"))+int(menu.getTextboxCont("ending day"))+(12-int(menu.getTextboxCont("starting month")))*31+(int(menu.getTextboxCont("ending year"))-int(menu.getTextboxCont("starting year"))-1)*12*31+(int(menu.getTextboxCont("ending month"))-1)*31);
  startDay=(31-int(menu.getTextboxCont("starting day"))+timeEndDay+(12-int(menu.getTextboxCont("starting month")))*31+(timeEndYear-int(menu.getTextboxCont("starting year"))-1)*12*31+(timeEndMonth-1)*31);
  endDay=(31-int(menu.getTextboxCont("starting day"))+timeStartDay+(12-int(menu.getTextboxCont("starting month")))*31+(timeStartYear-int(menu.getTextboxCont("starting year"))-1)*12*31+(timeStartMonth-1)*31);
  bufferMonth=int(menu.getTextboxCont("starting month"));
  bufferYear=int(menu.getTextboxCont("starting Year"));
  while (buffer<cantDias)
  {
    stroke(150, 150, 150);
    line(map(buffer, 0, cantDias, 300, render.width+300), height-7, map(buffer, 0, cantDias, 300, render.width+300), height);
    map(buffer, 0, cantDias, 300, render.width+300);
    if (bufferMonth==12)
    {
      bufferYear=bufferYear+1;
      fill(150, 150, 150);
      textSize(10);
      text(bufferYear, map(buffer, 0, cantDias, 300, render.width+300)-3, height-12); 
      line(map(buffer, 0, cantDias, 300, render.width+300), height-9, map(buffer, 0, cantDias, 300, render.width+300), height);
      bufferMonth=0;
    }  
    bufferMonth++;
    buffer=buffer+31;
  }
  fill(204, 102, 0);
  setGradient(300, height, render.width, -5, color(255, 0, 0), color(0, 200, 0), 2);
  stroke(255, 255, 255, 100);
  if (!manualAnimation)
    stroke(255, 0, 0);
  line(map(endDay, 0, cantDias, 300, render.width+300), height-11, map(endDay, 0, cantDias, 300,render.width+300), height);
  if (manualAnimation)
  {
    stroke(255, 0, 0);
    timeStartDay=int(menu.getTextboxCont("starting day"));
    timeStartMonth=int(menu.getTextboxCont("starting month"));
    timeStartYear=int(menu.getTextboxCont("starting year"));
    line(constrain(mouseX, 300, render.width+300), height-11, constrain(mouseX, 300, render.width+300), height);
    timeStartDay=int(map(constrain(mouseX, 300, render.width+300), 300, render.width+300, int(menu.getTextboxCont("starting day")), cantDias+int(menu.getTextboxCont("starting day"))));
  }
  fill(204, 102, 0);
  cantDias=0;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void setGradient(int x, int y, float w, float h, color c1, color c2, int axis ) //dibuja rectangulos en gradiene
{
  noFill();
  if (axis == 1)  // Top to bottom gradient
    for (int i = y; i <= y+h; i++) 
    {
      float inter = map(i, y, y+h, 0, 1);
      color c = lerpColor(c1, c2, inter);
      stroke(c);
      line(x, i, x+w, i);
    }
  else if (axis == 2)   // Left to right gradient
    for (int i = x; i <= x+w; i++) 
    {
      float inter = map(i, x, x+w, 0, 1);
      color c = lerpColor(c1, c2, inter);
      stroke(c);
      line(i, y, i, y+h);
    }
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void mousePressed()//activar los triggers que correspondan por donde se precione el mouse
{
  resetting_cam = false;
  boolean something_pressed = false;
  for (int i = 0; i < gumcubes.size(); i ++) 
  {
    if (gumcubes.get(i).checkMouse()) 
      something_pressed = true;
    if (gumcubes.get(i).active) 
      unSelectCubes(i);
  }
  if (mouseX > 300  && ! something_pressed && mouseY < height-10) 
    mouse_cam = true;
  if (mouseX > 300  && ! something_pressed && mouseY > height-10) 
    manualAnimation=true;
  menu.checkMouse();
  ruler.mouse();
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void mouseReleased()//desactivar triggers condo se suelta el mouse
{
  menu.release();
  for (int i = 0; i < gumcubes.size(); i ++) 
    gumcubes.get(i).release();
  mouse_cam = false;
  manualAnimation=false;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void mouseWheel(MouseEvent event) 
{
  float aux = event.getCount();
  if (mouseX > 300)
  {
    zoom -= aux*3;
    resetting_cam = false;
  }
  else if (mouseX < 300)
    menu.manageScroll(aux*3);
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void keyPressed() //check comandos por teclas
{
  if (!menu.checkKeys())//solo se checkean los comandos por teclas cuando no se esta escribiendo en algun textbox
  { 
    checkForViews();
    
    if (key == 's' || key =='S') 
    {
      String filename = "screenshots/capture-" + year() + "-" + nf(month(), 2) + "-" + nf(day(), 2) + "_"  + nf(hour(), 2) + "." + nf(minute(), 2) + nf(second(), 2) + ".png";
      saveFrame(filename);
      println("saved " + filename);
    } 
    else if (keyCode == BACKSPACE || keyCode == DELETE || keyCode == 127)
      attemptSubcubeDelete();
    else if (key == 'i' || key =='I') 
      attemptSubcubeInfo(); 
    else if ((key == 'r' || key =='R')) 
    {
      record = true;
      println("Record On");
    }
  }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void resetCamera() 
{
  zoom = init_zoom;
  rot_x = init_rot_x ;
  rot_y = init_rot_y ;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void fireCameraReset() ///iniciar el reseteo de la camara
{
  resetting_cam = true;
  cam_lerp_control = 0;
  rot_x_aux = rot_x;
  rot_y_aux = rot_y;
  zoom_aux = zoom;
}

//•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••

void addSubCube() //crear subcubo en una ubicacion al azar
{
  int x = int(random(-200, 200));
  int y = int(random(-200, 200));
  int z = int(random(-200, 200));
  GumballCube cube = new GumballCube(x, y, z);
  cube.asignColors();
  cube.setOffset(-300, 0);
  gumcubes.add(cube);
}

//•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••

void unSelectCubes(int index) //deseleccionar subcubos
{
  for (int i = 0; i < gumcubes.size(); i ++) 
    if (i!=index) 
      gumcubes.get(i).active = false;
}

//•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••

void attemptSubcubeDelete() //borrar subcubos seleccionados
{
  for (int i = 0; i < gumcubes.size(); i ++) 
    if (gumcubes.get(i).active)
      gumcubes.remove(i);
}

void attemptSubcubeInfo() //mostrar informacion de los subcubos seleccionados
{
  for (int i = 0; i < gumcubes.size(); i ++)
    if (gumcubes.get(i).active) 
      gumcubes.get(i).display_info  = ! gumcubes.get(i).display_info;
}

void drawDem()//not in use
{
  float x ;
  float y ;
  float z ;
  for (int j = def; j < filas-def; j=j+def)
    for (int i = 0; i < columnas-def; i=i+def) 
    {
      x =  map(coordx/1000, c.min_lon_map, c.max_lon_map, -cube_side/2, cube_side/2);
      y =  map(1, c.min_lon_map, c.max_lon_map, -cube_side/2, cube_side/2);
      z =  map(coordy/1000, c.min_lat_map, c.max_lat_map, -cube_side/2, cube_side/2);
      if (alt[j][i]!=-1&&y2*100<=0)
      {
        render.stroke(map(alt[j][i], minAlt, maxAlt, 0, 255), 0, 255-map(alt[j][i], minAlt, maxAlt, 0, 150));
        render.line(-(filas-j)/scale, -alt[j][i]/5/scale, (columnas-i)/scale, -x2/scale, y2/scale, -z2/scale);
      }
      x2=(filas-j);
      y2=-alt[j][i]/5;
      z2=-(columnas-i);
    }

  for (int i = def; i < columnas; i=i+def) 
    for (int j = def; j < filas; j=j+def)
    {
      if (alt[j][i]!=-1&&y2*100<=0)
      {
        render.stroke(map(alt[j][i], minAlt, maxAlt, 0, 255), 0, 255-map(alt[j][i], minAlt, maxAlt, 0, 255));
        render.line(-(filas-j)/scale, -alt[j][i]/5/scale, (columnas-i)/scale, -x2/scale, y2/scale, -z2/scale);
      }
      x2=(filas-j);
      y2=-alt[j][i]/5;
      z2=-(columnas-i);
    }
}  