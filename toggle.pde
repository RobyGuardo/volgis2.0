class Toggle {

  float x;
  float y;
  float w = 10;
  boolean pressed = false;
  String id= "";
  PFont font;
  boolean flag = false;

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  Toggle(float x_, float y_, String id_) //crear toggle
  {
    x = x_;
    y = y_;
    id = id_;
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  Toggle(float x_, float y_, float w_, String id_) //crear toggle
  {
    x = x_;
    y = y_;
    w = w_;
    id = id_;
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void render() {//dibujar
    pushStyle();
    textAlign(LEFT, TOP);
    noStroke();
    fill(85);
    rect(x, y+1, w, w, 4);
    fill(70);
    rect(x, y, w, w, 4);


    if (pressed)
      renderCheck();
    if(font!=null)
    textFont(font);
    fill(200);
    text(id, x+20, y);
    popStyle();
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void renderCheck() {//dibujar simbolo de activadt
    fill(2, 162, 161);
    rect(x+2, y+2, 6, 6, 2);
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void setFont(PFont f) {
    font = f;
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void checkMouse() {//check if mouse pressed on top of toggle
    if (mouseX > x && mouseX < x+ w && mouseY >y && mouseY < y + w) {
      pressed = !pressed;
      if (pressed)
        flag = true;
    }
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  boolean isPressed() {//averiguar estado
    return pressed;
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void check() {//activar
    pressed = true;
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void uncheck() {//desactivar
    pressed = false;
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void unflag() {//sacar flag
    flag = false;
  }
  
  
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  boolean getFlag() {//averiguar si tiene un flag
    return flag;
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void setPosition(int x_, int y_) {
    x = x_;
    y = y_;
  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  void setId(String s) {

    id = s;
  }


  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  String getId() {

    return id;
  }
  
  //*****************************************************************

  void move(int dx, int dy) {
    x += dx;
    y += dy;
  }
}
