# VolGIS 2.0

## VolGIS version 2.0

Last update: **September 2018**
Presented at _Cities on Volcanoes 10_

#### Available Features:

 - Preliminary density analysis;
 - Magnitude threshold visualisation;
 - High seismicity isosurfaces generation;
 - Overlay maps;
 - Crop box;
 - Subcubes;
 - 4D Analysis.



____

## **BUGS**
_(to be fixed asap)_


1. Switching from ED50 to WGS84 projection changes the grid parameters (from 51779 to 54719);
2. The 4D analysis panel shows 2813 events instead of 2824;
3. Ruler does not measure properly;
4. Maxima-minima analysis combined with 4D analysis freez the app.

____
____

### Further -major- implementations

1. Global extent (basic function automatically extended to all volcanoes);
2. Upload shapefile (*.shp) module;
3. Depth marker;
4. Edit magnitude graphics (by size);
4. Rename in a clearer way all the tabs;
5. Move the environment with the arrow keys;

____

#### Further -minor- implementations
1. Comments all the code in english _(preferably british)_;
2. Level transparency;
3. Set the title at each analysis;

